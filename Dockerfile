FROM python:3-alpine

ADD requirements.txt /requirements.txt

RUN apk add --no-cache build-base libxslt libxml2 libxml2-dev libxslt-dev && \
    pip install -U -r /requirements.txt && rm -v /requirements.txt && \
    apk del            build-base libxml2-dev libxslt-dev


ADD . /app

CMD ["python3", "/app/bridge.py"]
