#!/usr/bin/env python3

import json
import os
import re
import time
import urllib.parse
from datetime import datetime

import requests
from bs4 import BeautifulSoup as bs4

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
CACHE_PATH = os.path.join(THIS_DIR, "cache")
CACHE_TIME = 86400  # 1 day

WEB_ROOT = "http://www.gzmusica.com/"


def parse_file(fname):
    with open(fname, "rt") as f:
        doc = bs4(f.read(), "lxml")

    CALL_TO_ACTION = "Preme para obteres máis info"
    table = doc.find(class_="cal_table")
    days = table.find_all("td", class_="cal_td_daysnoevents") + table.find_all(
        "td", class_="cal_td_today"
    )
    data = []

    for td in days:
        daynum = td.find("a", class_="cal_daylink").text
        for event in td.find_all("a", class_="cal_titlelink"):
            title = event.text.strip()
            if re.match(r"^\d{1,2}:\d{1,2}", title):
                time, title = title.split(" ", 1)
            else:
                time = None

            link = event["href"]
            location = (
                bs4(event.parent["title"], "lxml")
                .text.replace(title, "")
                .replace(CALL_TO_ACTION, "")
                .strip()
            )
            data.append(
                {
                    "day": int(daynum),
                    "link": urllib.parse.urljoin(WEB_ROOT, link),
                    "location": location,
                    "time": time,
                    "title": title,
                }
            )

    return sorted(data, key=lambda d: d["day"])


def time_since_edition(fname):
    return time.time() - os.stat(fname).st_mtime


def get_month_file(year, month):
    fname = os.path.join(CACHE_PATH, str(year), str(month) + ".html")
    if os.path.exists(fname) and time_since_edition(fname) < CACHE_TIME:
        return fname
    os.makedirs(os.path.dirname(fname), exist_ok=True)
    req = requests.get(
        "http://www.gzmusica.com/axenda/este-mes/month.calendar/{}/{}/1/-.html".format(
            year, month
        )
    )
    with open(fname, "wt") as f:
        f.write(req.text)
    return fname


def get_data_from_month(year, month):
    fname = get_month_file(year, month)
    return parse_file(fname)


if __name__ == "__main__":
    now = datetime.now()
    data = get_data_from_month(now.year, now.month)
    print(json.dumps(data, indent=4))
