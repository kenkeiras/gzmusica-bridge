import logging
import os
from datetime import datetime

from programaker_bridge import BlockArgument  # Needed for argument definition
from programaker_bridge import ProgramakerBridge  # Import bridge functionality

from gzmusica import get_data_from_month

# Create the bridge object
bridge = ProgramakerBridge(
    name="GZMusica",
    endpoint=os.environ["BRIDGE_ENDPOINT"],
    token=os.environ["BRIDGE_TOKEN"],
    is_public=os.environ.get("PUBLIC_BRIDGE", "0") == "1",
)


@bridge.getter(
    id="get_current_month_remaining_events",
    message="Get this month remaining events",
    arguments=[],
    block_result_type=list,
)
def get_current_month_remaining_events(extra_data):
    now = datetime.now()

    data = get_data_from_month(now.year, now.month)

    remaining = []
    for event in data:
        if event["day"] >= now.day:
            remaining.append(event)
    return remaining


@bridge.getter(
    id="get_event_day",
    message="Get %1 day",
    arguments=[BlockArgument("struct", '<< Add here a "event" variable block >>')],
    block_result_type=int,
)
def get_event_day(event, extra_data):
    return event["day"]


@bridge.getter(
    id="get_event_title",
    message="Get %1 title",
    arguments=[BlockArgument("struct", '<< Add here a "event" variable block >>')],
    block_result_type=str,
)
def get_event_title(event, extra_data):
    return event["title"]


@bridge.getter(
    id="get_event_location",
    message="Get %1 location",
    arguments=[BlockArgument("struct", '<< Add here a "event" variable block >>')],
    block_result_type=str,
)
def get_event_day(event, extra_data):
    return event["location"]


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    logging.info("Starting bridge")
    bridge.run()  # Launch the bridge
